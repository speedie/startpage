#!/bin/sh
# Simple script for speedwm-swal which copies the current wallpaper to img/wallpaper and sets colors.
# Licensed under GNU GPLv3.

PREFIX="$HOME/.config/qutebrowser" # Prefix
CWAL="$HOME/.config/speedwm/swal/CurrentWallpaper" # Symlink to the current wallpaper

# Real path for current wallpaper
command -v speedwm-swal > /dev/null && \
		IPATH="$(readlink "$CWAL")"

# Copy current wallpaper to img/wallpaper
if [ -e "$IPATH" ]; then
		/bin/cp "$IPATH" "$PREFIX/startpage/img/wallpaper"
else
		printf "Wallpaper is not available, skipping.\n"
fi

# Color for links
. "$PREFIX/startpage/colors"

# Set colors based on wal/colors
if [ -e "$HOME/.cache/wal/colors" ]; then
		LINKCOLOR="$(sed -n 7,7p $HOME/.cache/wal/colors)"
		PARACOLOR="$(sed -n 2,2p $HOME/.cache/wal/colors)"
		HEADCOLOR="$(sed -n 4,4p $HOME/.cache/wal/colors)"
		TXSXCOLOR="$(sed -n 8,8p $HOME/.cache/wal/colors)"
		BGSXCOLOR="$(sed -n 13,13p $HOME/.cache/wal/colors)"
fi

printf "a {\n  color: ${LINKCOLOR};\n}\n" > "$PREFIX/startpage/fontcol.css"
printf "h1 {\n  color: ${HEADCOLOR};\n}\n" >> "$PREFIX/startpage/fontcol.css"
printf "p {\n  color: ${PARACOLOR};\n}\n" >> "$PREFIX/startpage/fontcol.css"
printf "input[type=search] {\n  -webkit-box-shadow: 0 0 0px 1000px ${BGSXCOLOR} inset;\n  -webkit-text-fill-color: ${TXSXCOLOR} !important;\n}\n" >> $PREFIX/startpage/fontcol.css
