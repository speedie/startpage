# startpage

JS and PHP free minimal startpage with Pywal and dynamic wallpaper
support meant to be used locally

![image](startpage.png?raw=true "image")

## Usage with qutebrowser

Clone the repository into ~/.config/qutebrowser and add these two lines to config.py.

`config.set('url.start_pages','~/.config/qutebrowser/startpage/index.html')`
`config.set('url.default_page','~/.config/qutebrowser/startpage/index.html')`

You don't need to edit script-qutebrowser.sh if you're using speedwm as it is default.

## Usage with Chromium based browsers
Clone the repository into ~/.config and set the start page (I'll assume you know how to do this on Chromium) and then see 'To copy in the wallpaper'

For pywal support in the browser (as a theme), clone https://github.com/metafates/ChromiumPywal into ~/.config/startpage/. See that repo for more information on how to use it.

## Usage with Firefox based browsers
Clone the repository into ~/.config and set the start page. Again I'm going to assume you know how to do this for Firefox. Now see 'To copy in the wallpaper'

For pywal support in the browser, install https://github.com/frewacom/pywalfox!

## Usage with suckless surf
Clone the repository into ~/.config and make sure surf has the [homepage](https://surf.suckless.org/patches/homepage) patch applied.

Change the value of HOMEPAGE in config.h to ~/.config/startpage/index.html. Now see 'To copy in the wallpaper'.

## Usage with Chromium based browsers

Clone the repository into ~/.config and set the start page (I'll assume you
know how to do this on Chromium) and then see 'To copy in the wallpaper'

## Usage with Firefox based browsers

Clone the repository into ~/.config and set the start page (Again I'll assume
you know how to do this)

## Usage for other browsers

Clone the repository and edit script-generic.sh and change PREFIX if you aren't using speedwm.
Check the wiki on how to set a start page for your specific browser.

## To copy in the wallpaper

If you're using speedwm-extras, you can copy the script to ~/.config/speedwm/swal/postrun.sh.

Note that you need to be using speedwm-swal > 1.2 for this.
Otherwise find some other method to copy the wallpaper to img/wallpaper.

## Pywal

If you're using Pywal, this script also allows you to override font colors with
pywal colors. Simply run the script and make sure Pywal works.

## License

License for the shell script is GNU GPLv3.
License for the HTML and this README is Creative Commons
Attribution-ShareAlike 4.0 International.

## Credit

- speedie - Wrote the main page
- emilyd  - Search function
